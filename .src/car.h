#ifndef CAR_H
#define CAR_H
#include <iostream>
using namespace std;

class Car {
	public:
		//##Constructors##
		Car();	//default Constructor
		Car(int baseprice, string modelname, string extcolor, string fabric, bool brakes, bool tranny, int finalprice); //Allow Access to private vars

		virtual void print();	//returns car info

		//##Set function Headers##
		void setBasePrice(int baseprice);
		void setModelName(string modelname);
		void setExtColor(string extcolor);
		void setFabric(string fabric);
		void setBrakes(bool brakes);
		void setTranny(bool tranny);
		void setFinalPrice(int finalprice);

		//##Return Values##
		int getBasePrice() {
			return baseprice;
		}
		string getModelName() {
			return modelname;
		}
		string getExtColor() {
			return extcolor;
		}
		string getFabric() {
			return fabric;
		}
		bool getBrakes() {
			return brakes;
		}
		bool getTranny() {
			return transmission;
		}
		int getFinalPrice(){
			return finalprice;
		}

	private:
		int baseprice;
		string modelname;
		string extcolor;
		string fabric;
		bool brakes;
		bool transmission;
		int finalprice;
};
#endif
