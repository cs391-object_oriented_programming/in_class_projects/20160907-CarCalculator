#include "car.h"
#include <iostream>
using namespace std;

Car::Car() { //default constructor ***HAVE TO USE .h VARIABLE NAMES***
	baseprice = 0;
	modelname = "";
	extcolor = "";
	fabric = "";
	brakes = 0;
	transmission = 0;
	finalprice = 0;
}

Car::Car(int baseprice, string modelname, string extcolor, string fabric, bool brakes, bool tranny, int finalprice) :Car() { //Constructor Delegation. calls the default constructor above
	/*Value pointer from main.cpp to car.cpp*/
	this->baseprice=baseprice;
	setBasePrice(baseprice);

	this->modelname=modelname;
	setModelName(modelname);

	this->extcolor=extcolor;
	setExtColor(extcolor);

	this->fabric=fabric;
	setFabric(fabric);

	this->brakes=brakes;
	setBrakes(brakes);

	this->transmission=tranny;
	setTranny(tranny);

	this->finalprice=finalprice;
	setFinalPrice(finalprice);
}

//***SET CONSTRUCTOR VARIABLE TO .h ONE***
void Car::setBasePrice(int baseprice) {
	if(baseprice>=0)this->baseprice=baseprice;
}
void Car::setModelName(string modelname) {
	if(modelname!="")this->modelname=modelname;
}
void Car::setExtColor(string extcolor) {
	if(extcolor!="")this->extcolor=extcolor;
}
void Car::setFabric(string fabric) {
	if(fabric!="")this->fabric=fabric;
}
void Car::setBrakes(bool brakes) {
	if(brakes>=0)this->brakes=brakes;
}
void Car::setTranny(bool tranny) {
	if(transmission>=0)this->transmission=tranny;
}
void Car::setFinalPrice(int finalprice) {
	if(finalprice>=0)this->finalprice=finalprice;
}

void Car::print() {	//dynamic car info output
	 string br="", tr="";

	(brakes==1)?br="Ceramic":br="Standard";
	(transmission==1)?tr="Auto":tr="Manual";

	printf("#Base Price#: %i, #Model#: %s, #Color#: %s, #Fabric#: %s, #Brakes#: %s, Transmission: %s, Final Price: %i \n",
	       baseprice, modelname.c_str(), extcolor.c_str(), fabric.c_str(), br.c_str(), tr.c_str(), finalprice);
}
