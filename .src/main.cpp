#include <iostream>
#include <string>
#include "car.h"
using namespace std;

void BrakeTrannyHandler(string, string, bool&, bool&);
void fabriculator(int, string&);
int ComputePrice(int, string , bool, bool);	//returns final price

int main() {
	Car *car1 = new Car(0, " ", " ", " ", 0, 0, 0);	//Instantiate new car object
	cout << "\t\t\t####Car Price Calculator###\n";

	int bp=0;
	cout << "\nEnter Base Price: ";
	cin >> bp;
	car1->setBasePrice(bp);

	string mn="";
	cout << "Enter Model Name: ";
	cin.get();	//needed because of friggin cin>>
	getline(cin, mn);
	car1->setModelName(mn);

	string clr="";
	cout << "Enter Color: ";
	getline(cin, clr);
	car1->setExtColor(clr);

	int ifab=0;
	string sfab="";
	cout << "Type 1, 2 or 3 to select the type of Fabric."
	     << "\n\t(1) Standard (+$000)"
	     << "\n\t(2) Premium  (+$150)"
	     << "\n\t(3) Leather  (+$300)\n";
	cin >> ifab;
	fabriculator(ifab,sfab);	//menu selection to string
	car1->setFabric(sfab);

	string brin="";
	cout << "Do you want to add Ceramic Breakpads? ";
	cin.get();//cin>> fix
	getline(cin, brin);

	string trin="";
	cout << "Do you want to add Automatic Transmission? ";
	getline(cin, trin);

	bool brakes=0, trans=0;
	BrakeTrannyHandler(brin, trin, brakes, trans);
	car1->setBrakes(brakes);
	car1->setTranny(trans);

	//###Final Price Output###
	int final=0;
	car1->setFinalPrice(final);	//set car.cpp final price
	car1->print();	//output car.cpp data

	cout << "\n\n\t\t\t";
	system("PAUSE");
	return 0;

	/*#Deprecated main.cpp output#
	final = ComputePrice(bp, sfab, brakes, trans);
	cout << "\n\tOriginal Price: " << car1->getBasePrice()
	     << "\n\tModel Name: " << car1->getModelName()
	     << "\n\tColor: " << car1->getExtColor()
	     << "\n\tFabric: " << car1->getFabric()
	     << "\n\tDiskBreaks: " << car1->getBrakes()
	     << "\n\tTransmission: " << car1->getTranny()
	     << "\n\tLegacy Final Price is: " << final
	     << "\n\tClass Car Final Price is: " << car1->getFinalPrice();*/
}

void BrakeTrannyHandler(string b, string t, bool &bo, bool &to) {
	string bt= "yY1";

	for (int x=0; x<bt.length(); x++) {	//check for yes/no using string
		if (b[0]==bt[x]) bo=1;
		if (t[0]==bt[x]) to=1;
	}

	//Possibly Redundant
	if (bo!=0&&bo!=1 || to!=0&&to!=1)
		cout << "\n\t\t/!\\ Something is wrong in BrakeTrannyHandler /!\\\n";

	//cout<<"\n\n\t\tB: "<<b[0]<<"\tT: "<<t[0]<<"\tbt Length: "<<bt.length()<<endl;
}


//Fabric int to string
void fabriculator(int fcin, string &fcout) {
	switch(fcin) {	//fabric options
		case 1:
			fcout = "Standard";
			break;
		case 2:
			fcout = "Premium";
			break;
		case 3:
			fcout = "Leather";
			break;
		default:
			cout << "\n\t/!\\Something is wrong in fabriculator/!\\\n\n";
			break;
	}
}
//##Price Calculator##
int ComputePrice(int bpriceIn, string fabIn, bool brkIn, bool tranIn) {
	int TnR=750, total=bpriceIn;

	string sF= fabIn;
	if (sF=="Standard") 	total+=000;
	else if (sF=="Premium") total+=150;
	else if (sF=="Leather") total+=300;
	else {
		cout << "\n\n\tInvalid Fabric string: " << sF << endl;
		return 0;
	}

	(tranIn==1) ? total+=1500 : total;	//Automatic Transmission costs $1500
	(brkIn==1) 	? total+=0200 : total;		//Ceramic Brakes cost $200
	total+=TnR;	//Taxes & registration

	return total;
}
